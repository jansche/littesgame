package littesGame;

import org.newdawn.slick.*;
import org.newdawn.slick.opengl.SlickCallable;

import java.util.ArrayList;
import java.util.List;

public class LittesGame extends BasicGame {

    private List<Actor> actors;
    Protagonist protagonist;
    private Game game;
    Input input;

    float redDelta = 0;
    float redDeltaMax = 500;
    float blueDelta = 0;
    float blueDeltaMax = 2000;

    static int screenWidth = 1200;
    static int screenHeight = 800;

    public static void main(String[] argv) {
        try {
            AppGameContainer container = new AppGameContainer(new LittesGame("Littes Game"));
            container.setDisplayMode(screenWidth,screenHeight,false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    public LittesGame(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        this.actors = new ArrayList<>();
        gameContainer.setTargetFrameRate(59);
        this.protagonist = new Protagonist(screenWidth / 2, screenHeight - 100);
        game = this;

    }

    @Override
    public void update(GameContainer gameContainer, int delta) throws SlickException {
        for (Actor actor : this.actors) {
            actor.update(gameContainer, delta);
            if(actor.getHitbox().intersects(protagonist.getHitbox())) {
                System.out.println("aehgrr");
                
            }
        }
        protagonist.update(gameContainer, delta);

        this.redDelta += delta;
        if(this.redDelta >= this.redDeltaMax) {
            this.actors.add(new RedMeteor((float)Math.random() * (screenWidth),-20));
            this.redDelta = 0;
        }

        this.blueDelta += delta;
        if(this.blueDelta >= this.blueDeltaMax) {
            this.actors.add(new BlueMeteor((float)Math.random() * (screenWidth),-20));
            this.blueDelta = 0;
        }


        input = gameContainer.getInput();
        if(input.isKeyDown(Input.KEY_LEFT)) {
            this.protagonist.moveLeft();
        }  if(input.isKeyDown(Input.KEY_RIGHT)) {
            this.protagonist.moveRight();
        }  if(input.isKeyDown(Input.KEY_UP)) {
            this.protagonist.moveUp();
        }  if(input.isKeyDown(Input.KEY_DOWN)) {
            this.protagonist.moveDown();
        }  if(input.isKeyDown(Input.KEY_A)) {
            this.protagonist.moveLeft();
        }  if(input.isKeyDown(Input.KEY_D)) {
            this.protagonist.moveRight();
        }  if(input.isKeyDown(Input.KEY_W)) {
            this.protagonist.moveUp();
        }  if(input.isKeyDown(Input.KEY_S)) {
            this.protagonist.moveDown();
        }
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        for (Actor actor : this.actors) {
            actor.render((graphics));
        }
        protagonist.render(graphics);
    }
}
