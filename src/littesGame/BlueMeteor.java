package littesGame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;

public class BlueMeteor implements Actor{
    private float x,y;
    private float myDelta = 0;
    private float maxDelta = 5;
    private Shape hitbox;
    Image image;


    public BlueMeteor(float x, float y) {
        this.x = x;
        this.y = y;
        this.hitbox = new Circle(x,y,30);
        try{
            this.image = new Image("./assets/blue.png");
        } catch(Exception e) {
            System.out.println("Bild nicht gefunden.");
        }
    }

    @Override
    public void render(Graphics graphics) {
        //graphics.drawOval(this.x,this.y,20,20);
        graphics.drawImage(this.image,x,y);
    }

    public Shape getHitbox() {
        return this.hitbox;
    }

    @Override
    public void update(GameContainer gameContainer, int delta) {
        this.myDelta += delta;
        if(this.myDelta >= this.maxDelta) {
            this.y = this.y + 3;
            this.myDelta = 0;
        }
        this.hitbox.setLocation(x,y);
    }
}
