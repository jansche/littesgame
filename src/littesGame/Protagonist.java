package littesGame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Protagonist implements Actor {

    private float x,y;
    private Shape hitbox;
    private Image image;


    public Protagonist(float x, float y) {
        this.x = x;
        this.y = y;
        this.hitbox = new Rectangle(x,y,30,30);
        try{
            this.image = new Image("./assets/ship.png");
        } catch(Exception e) {
            System.out.println("Bild nicht gefunden.");
        }
    }
    public Shape getHitbox() {
        return this.hitbox;
    }

    public void moveLeft() {
        this.x -= 10;
    }

    public void moveRight() {
        this.x += 10;
    }

    public void moveUp() {
        this.y -= 10;
    }

    public void moveDown() {
        this.y += 10;
    }

    @Override
    public void render(Graphics graphics) {
        //graphics.drawRect(this.x,this.y,30,30);
        graphics.drawImage(this.image,x,y);
    }

    @Override
    public void update(GameContainer gameContainer, int delta) {
        this.hitbox.setLocation(this.x,this.y);
    }
}
