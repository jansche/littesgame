package littesGame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public interface Actor {
    
    void render(Graphics graphics);
    void update(GameContainer gameContainer, int delta);
    Shape getHitbox();

}
